# Copyright 2009 Jan Meier
# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist=none test=pytest ]

SUMMARY="Jinja2 is a template engine written in pure Python"
HOMEPAGE="https://palletsprojects.com/p/jinja/"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="vim-syntax"

DEPENDENCIES="
    build+run:
        dev-python/MarkupSafe[>=0.23][python_abis:*(-)?]
"

prepare_one_multibuild() {
    # upstream doesn't care to fix this, see https://github.com/pallets/jinja/issues/{643,653,655}
    if [[ $(python_get_abi) == 2.7 ]] || [[ $(python_get_abi) == 3.4 ]] || [[ $(python_get_abi) == 3.5 ]] ; then
        edo rm jinja2/async{filters,support}.py
    fi

    setup-py_prepare_one_multibuild
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    if option vim-syntax; then
        insinto /usr/share/vim/vimfiles/syntax
        doins ext/Vim/*
    fi
}

