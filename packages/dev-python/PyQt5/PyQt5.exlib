# Copyright 2008-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'PyQt4-4.4.4-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

myexparam sip_version

require pypi
require python [ blacklist=none multiunpack=true ]

SUMMARY="PyQt5 is a set of Python bindings for the Qt5 toolkit"
DESCRIPTION="
PyQt is a set of Python bindings for the Qt application framework and runs
on all platforms supported by Qt including Windows, MacOS/X and Linux. The
bindings are implemented as a set of Python modules and contain over 300
classes and over 6,000 functions and methods.
"
BASE_URI="https://www.riverbankcomputing.com"
HOMEPAGE="${BASE_URI}/software/pyqt/download5/"
DOWNLOADS="${BASE_URI}/static/Downloads/${PN}/${PV}/${PNV}.tar.gz"

BUGS_TO="philantrop@exherbo.org"

UPSTREAM_CHANGELOG="${BASE_URI}/static/Downloads/${PN}/ChangeLog"
UPSTREAM_RELEASE_NOTES="${BASE_URI}/news/pyqt-$(ever major)$(ever range 2)"
UPSTREAM_DOCUMENTATION="${BASE_URI}/static/Docs/${PN}/ [[ lang = en ]]"

SLOT="0"
LICENCES="|| ( GPL-2 GPL-3 )"
MYOPTIONS="dbus debug
    bluetooth     [[ description = [ Build bindings for QtBluetooth & QtNfc ] ]]
    multimedia    [[ description = [ Build bindings for QtMultimedia ] ]]
    networkauth   [[ description = [ Build bindings for QtNetworkAuth ] ]]
    positioning   [[ description = [ Build bindings for QtPositioning ] ]]
    remoteobjects [[ description = [ Build bindings for QtRemoteObjects ] ]]
    sensors       [[ description = [ Build bindings for QtSensors ] ]]
    serialport    [[ description = [ Build bindings for QtSerialPort ] ]]
    sql           [[ description = [ Build bindings for QtSql ] ]]
    webchannel    [[ description = [ Build bindings for QtWebChannel ] ]]
    webkit        [[ description = [ Build bindings for QtWebKit ] ]]
    websockets    [[ description = [ Build bindings for QWebSockets ] ]]
    x11extras     [[ description = [ Build bindings for QtX11Extras ] ]]
"

DEPENDENCIES="
    build+run:
        dev-python/sip[>=$(exparam sip_version)][python_abis:*(-)?]
        x11-libs/qtbase:5
        x11-libs/qtdeclarative:5
        x11-libs/qtsvg:5
        x11-libs/qttools:5 [[ note = [ QtHelp ] ]]
        x11-libs/qtxmlpatterns:5
        bluetooth? ( x11-libs/qtconnectivity:5 )
        dbus? (
            dev-python/dbus-python[python_abis:*(-)?]
            sys-apps/dbus
        )
        multimedia? ( x11-libs/qtmultimedia:5 )
        networkauth? ( x11-libs/qtnetworkauth:5 )
        positioning? ( x11-libs/qtlocation:5 )
        python_abis:2.7? ( dev-python/enum34[python_abis:2.7] )
        remoteobjects? ( x11-libs/qtremoteobjects:5 )
        sensors? ( x11-libs/qtsensors:5 )
        serialport? ( x11-libs/qtserialport:5 )
        sql? ( x11-libs/qtbase:5[sql] )
        webchannel? ( x11-libs/qtwebchannel:5 )
        webkit? ( x11-libs/qtwebkit:5 )
        websockets? ( x11-libs/qtwebsockets:5 )
        x11extras? ( x11-libs/qtx11extras:5 )
"

DEFAULT_SRC_INSTALL_PARAMS=( INSTALL_ROOT="${IMAGE}" )

pyqt_enable() {
    local flag=${1,,} module=${2:-Qt${1}}

    if option "${flag}" ; then
        echo "--enable=${module}"
    fi
}

# Unused modules:
# QAxContainer: Qt's ActiveX and COM support (qtactiveqt)
# QtAdndroidExtras, QtMacExtras, QtWinExtras: Platform specific components for
#    Android, Mac OS, Windows
PYQT_CONFIGURE_OPTION_ENABLES=(
    Bluetooth
    "bluetooth QtNfc"
    DBus
    Multimedia
    "multimedia QtMultimediaWidgets"
    "networkauth QtNetworkAuth"
    "remoteobjects QtRemoteObjects"
    Sensors
    SerialPort
    Sql
    "webchannel QtWebChannel"
    WebKit
    "webkit QtWebKitWidgets"
    "websockets QtWebSockets"
    X11Extras
)
PYQT_CONFIGURE_ALWAYS_ENABLES=(
    QtCore
    QtGui
    QtHelp
    QtNetwork
    QtOpenGL
    QtPrintSupport
    QtQml
    QtQuick
    QtQuickWidgets
    QtSvg
    QtTest
    QtWidgets
    QtXml
    QtXmlPatterns
)
PYQT_CONFIGURE_PARAMS=(
        # Disable PyQt API file for QScintilla
        --no-qsci-api
        # Disable QtDesigner support. Enable with: --enable=QtDesigner --enable=uic
        --no-designer-plugin
)

prepare_one_multibuild() {
    python_prepare_one_multibuild

    if option dbus; then
        edo sed -e "s:\(cmd.*=.*\)\(pkg-config\):\1$(exhost --tool-prefix)pkg-config:" \
                -i configure.py
    else
        edo sed -e '/pkg-config.*dbus-1/s/sout.*$/return # DBus support disabled/' -i configure.py
    fi

    # When system python is set to 2.6 python_bytecompile() can't process
    # source files that use python 3 syntax. See Gentoo bug #274499.
    [[ $(python_get_abi) == 2.* ]] && edo rm -rf pyuic/uic/port_v3
    [[ $(python_get_abi) == 3.* ]] && edo rm -rf pyuic/uic/port_v2
}

configure_one_multibuild() {
    edo ${PYTHON} configure.py \
        $(option debug && echo '--debug') \
        $(option debug && echo '--qml-debug') \
        --confirm-license \
        --bindir /usr/$(exhost --target)/bin \
        --destdir $(python_get_sitedir) \
        --qmake /usr/$(exhost --target)/lib/qt5/bin/qmake \
        --sipdir /usr/share/sip/${PN} \
        --sip-incdir $(python_get_incdir) \
        --verbose \
        "${PYQT_CONFIGURE_PARAMS[@]}" \
        $(for s in ${PYQT_CONFIGURE_ALWAYS_ENABLES[@]} ; do echo "--enable=${s}" ; done) \
        $(for s in "${PYQT_CONFIGURE_OPTION_ENABLES[@]}" ; do pyqt_enable ${s} ; done)

    edo find "${WORK}" -name Makefile | xargs sed -i "/^\tstrip /d"
}

install_one_multibuild() {
    default
    python_bytecompile

    insinto /usr/share/doc/${PNVR}
    doins -r examples

    if [[ -d "${IMAGE}"/usr/lib ]]; then
        edo cp -a "${IMAGE}"/usr/lib/* "${IMAGE}"/usr/$(exhost --target)/lib/
        edo rm -r "${IMAGE}"/usr/lib
    fi

    # Kill empty dirs.
    edo find "${IMAGE}"/usr/ -type d -empty -delete
}

