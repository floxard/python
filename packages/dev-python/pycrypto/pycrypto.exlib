# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pycrypto-2.0.1-r6.ebuild' from Gentoo, which is:
#     Copyright 1999-2007 Gentoo Foundation

require pypi setup-py [ import='distutils' blacklist='none' ]

SUMMARY="Python Cryptography Toolkit"
DESCRIPTION="
The Toolkit is a collection of cryptographic algorithms and protocols,
implemented for use from Python. Among the contents of the package:
* Hash functions: MD2, MD4, RIPEMD, SHA256.
* Block encryption algorithms: AES, ARC2, Blowfish, CAST, DES, Triple-DES, IDEA,
  RC5.
* Stream encryption algorithms: ARC4, simple XOR.
* Public-key algorithms: RSA, DSA, ElGamal, qNEW.
* Protocols: All-or-nothing transforms, chaffing/winnowing.
* Miscellaneous: RFC1751 module for converting 128-key keys into a set of
  English words, primality testing.
* Some demo programs (currently all quite old and outdated).
"
HOMEPAGE+=" https://www.dlitz.net/software/${PN}"

BUGS_TO="alip@exherbo.org"
REMOTE_IDS+=" launchpad:${PN}"

UPSTREAM_CHANGELOG="https://github.com/dlitz/${PN}/blob/master/ChangeLog"

LICENCES="public-domain PSF-2.2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/gmp:=
        !dev-python/pycryptodome [[
            description = [ pycrypto and pycryptodome install colliding files ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/8dbe0dc3eea5c689d4f76b37b93fe216cf1f00d4.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-gmp
    --without-mpir
)

test_one_multibuild() {
    esandbox allow_net "unix:${TEMP}/pymp-*/listener-*"

    export PYTHONPATH=$(ls -d build/lib.*/)
    edo ${PYTHON} -B setup.py test

    esandbox disallow_net "unix:${TEMP}/pymp-*/listener-*"
}

