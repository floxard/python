# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

# Python 3 only because ipython is the only dependent.
require pypi setup-py [ blacklist=2 import=setuptools test=pytest ]

SUMMARY="Qt-based console for Jupyter with support for rich media output"
DESCRIPTION="
The Qt console is a very lightweight application that largely feels like a terminal,
but provides a number of enhancements only possible in a GUI, such as inline figures,
proper multi-line editing with syntax highlighting, graphical calltips, and much more.
The Qt console can use any Jupyter kernel.
"
HOMEPAGE="https://jupyter.org"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/ipykernel[>=4.1][python_abis:*(-)?]
        dev-python/ipython_genutils[python_abis:*(-)?]
        dev-python/jupyter_client[>=4.1][python_abis:*(-)?]
        dev-python/jupyter_core[python_abis:*(-)?]
        dev-python/Pygments[python_abis:*(-)?]
        dev-python/PyQt5[python_abis:*(-)?]
        dev-python/traitlets[python_abis:*(-)?]
"

PYTEST_PARAMS=(
    -vs
)

src_test() {
    DISPLAY= setup-py_src_test
}

